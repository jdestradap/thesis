package co.com.music.pruebaconcepto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLOntologyCreationIOException;
import org.semanticweb.owlapi.io.OWLParser;
import org.semanticweb.owlapi.io.OWLParserException;
import org.semanticweb.owlapi.io.UnparsableOntologyException;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.UnloadableImportException;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.util.OWLEntityRemover;

/**
 * Author: Juan Diego Estrada P�rez<br>
 * Universidad EAFIT<br>
 * Ingenier�a de Sistemas<br>
 * Date: 07-Aug-2011 </p> 
 * Implementa la l�gica necesaria para realizar 
 * operaciones sobre la ontolog�a y lograr inferir la tonalidad de una 
 * composici�n musical en formato MusicXML.
 * 
 * 
 */
public class PruebaConcepto {

    public static final String COMP_DIRECTORY = "composicionesMusicales";
    public static final String ONTOLOGY_FILE = "baseDeConocimiento/ontologyConceptTest.owl";

    public PruebaConcepto() {

    }

    public static void main(String[] args) throws OWLOntologyStorageException {
        if (args.length == 1 ) {

            File directory = new File(COMP_DIRECTORY);
            File[] files = directory.listFiles();
            String input = "";

            input = args[0] == null ? input : args[0];
            boolean execute = false;

            if (input.trim().equals("")) {
                System.out
                    .println("You must enter the composition name. Type \"help\" to see the list of compositions.");
            } else if (!input.equals("help")) {
                for (File comp : files) {
                    if (comp.getName().equals(input)) {
                        execute = true;
                        PruebaConcepto lo = new PruebaConcepto();
                        lo.pruebaConcepto(comp.getName());
                    }
                }

                if (!execute) {
                    System.out
                        .println("The composition is not found. Type \"help\" to see the list of compositions.");
                }
            }

            if (input.trim().equals("help")) {
                System.out.println("The compositions are:\n");
                for (File comp : files) {
                    System.out.println("- " + comp.getName() + "\n");
                }
            }
			
        } else {
            System.out
                .println("You must enter the composition name. Type \"help\" to see the list of compositions.");
        }
    }

    public void pruebaConcepto(String compositionName)
        throws OWLOntologyStorageException {

        try {

            String base = "http://www.semanticweb.org/ontologies/2011/6/Ontology1310933060709.owl";

            // Get hold of an ontology manager
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

            // Se carga la ontologia.

            IRI iri = IRI.create(new File(ONTOLOGY_FILE));

            OWLOntology musicOntology = manager
                .loadOntologyFromOntologyDocument(iri);

            OWLDataFactory dataFactory = manager.getOWLDataFactory();

            // Se crea una instancia de la clase MusicXMLReader, y utilizamos el
            // metodo getMusicNotes, el cual devuelve las notas en formato String.

            MusicXMLReader mxr = new MusicXMLReader();

            String tonicNote = "";
			
            ArrayList<String> notes = mxr.getMusicNotes(COMP_DIRECTORY + "/"
                                                        + compositionName);
			
            // Se obtiene la ultima nota de la partitura
            tonicNote = notes.get(notes.size() - 1) == null?tonicNote:notes.get(notes.size() - 1);
			
            System.out.println("The last note (tonic) is: " + tonicNote);

            ArrayList<OWLIndividual> owlNotes = new ArrayList<OWLIndividual>();

            System.out.println("The notes in the composition are: ");

            // Aqui se crean los individuos owl tipo nota.

            for (String note : notes) {

                System.out.println(note);

                OWLIndividual owlNote = dataFactory.getOWLNamedIndividual(IRI
                                                                          .create(base + "#" + note));

                owlNotes.add(owlNote);

            }


            OWLIndividual song = dataFactory.getOWLNamedIndividual(IRI
                                                                   .create(base + "#song"));

            //Se elimina la entidad song y todas las referencias a este individuo, en caso de que ya exista. 
            //Esto se hace con el fin de trabajar siempre con una nueva entidad song.
			
            OWLEntityRemover remover = new OWLEntityRemover(manager, Collections.singleton(musicOntology));
		
            for (OWLNamedIndividual indRmv : musicOntology.getIndividualsInSignature()) {
                if(indRmv.equals(song)){
                    indRmv.accept(remover);
                }
            }
			
            manager.applyChanges(remover.getChanges());
			
            remover.reset();
			
            // Se crea el individuo tipo tonic.
			
            OWLIndividual tonicNoteOWL = dataFactory.getOWLNamedIndividual(IRI
                                                                           .create(base + "#" + tonicNote));

            // Se crea la propiedad hasNote.

            OWLObjectProperty hasNote = dataFactory.getOWLObjectProperty(IRI
                                                                         .create(base + "#hasNote"));
			
            // Se crea la propiedad hasTonic
			
            OWLObjectProperty hasTonic = dataFactory.getOWLObjectProperty(IRI
                                                                          .create(base + "#hasTonic"));

            ArrayList<OWLObjectPropertyAssertionAxiom> axioms = new ArrayList<OWLObjectPropertyAssertionAxiom>();

            // Aqui se crean las proposiciones que relaciona cada nota con el
            // individuo song mediante
            // la propiedad hasNote.

            for (OWLIndividual owlNotetmp : owlNotes) {

                OWLObjectPropertyAssertionAxiom axiom = dataFactory
                    .getOWLObjectPropertyAssertionAxiom(hasNote, song,
                                                        owlNotetmp);
                axioms.add(axiom);
            }

            // Se crea la proposici�n que relaciona la ultima nota de la composici�n
            // musical (tonica) con el individuo song.
			
            OWLObjectPropertyAssertionAxiom axiom = dataFactory
                .getOWLObjectPropertyAssertionAxiom(hasTonic, song,
                                                    tonicNoteOWL);
			
            axioms.add(axiom);
			
            ArrayList<AddAxiom> changes = new ArrayList<AddAxiom>();

            for (OWLObjectPropertyAssertionAxiom axiomtmp : axioms) {

                AddAxiom addAxiomChange = new AddAxiom(musicOntology, axiomtmp);
                changes.add(addAxiomChange);
            }

            manager.applyChanges(changes);

            // Se guarda la ontologia con las nuevas afirmaciones.
			
            manager.saveOntology(musicOntology);

            /**
             * 
             * Se crea un razonador que va a razonar sobre nuestra ontolog�a.
             * 
             */

            OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();

            OWLReasoner reasoner = reasonerFactory
                .createReasoner(musicOntology);

            reasoner.precomputeInferences();

            // Se determina si la ontolog�a en realidad es consistente.

            boolean consistent = reasoner.isConsistent();
            System.out.println("Consistent: " + consistent);
            System.out.println("\n");

            // Preguntamos por los valores de la propiedad hasNote para el
            // individuo Song.

            NodeSet<OWLNamedIndividual> noteValuesNodeSet = reasoner
                .getObjectPropertyValues((OWLNamedIndividual) song, hasNote);
            Set<OWLNamedIndividual> values = noteValuesNodeSet.getFlattened();
            System.out.println("The hasNote property values for Song are: ");
            for (OWLNamedIndividual ind : values) {
                System.out.println("    " + ind);
            }

            // Preguntamos por los valores de la propiedad hasTonic para el
            // individuo Song.
			
            NodeSet<OWLNamedIndividual> tonicValuesNodeSet = reasoner
                .getObjectPropertyValues((OWLNamedIndividual) song, hasTonic);
            Set<OWLNamedIndividual> valuesTonic = tonicValuesNodeSet.getFlattened();
            System.out.println("The hasTonic property values for Song are: ");
            for (OWLNamedIndividual ind : valuesTonic) {
                System.out.println("    " + ind);
            }
			
            // Se determina el tipo de individuo Song.

            NodeSet<OWLClass> individualsNodeSet = reasoner.getTypes(
                                                                     (OWLNamedIndividual) song, true);

            Set<OWLClass> types = individualsNodeSet.getFlattened();
            System.out.println("Song Type: ");
            for (OWLClass type : types) {
                System.out.println("    " + type);
            }

            System.out.println("\n");

        } catch (OWLOntologyCreationIOException e) {
            // IOExceptions during loading get wrapped in an
            // OWLOntologyCreationIOException
            IOException ioException = e.getCause();
            if (ioException instanceof FileNotFoundException) {
                System.out.println("Could not load ontology. File not found: "
                                   + ioException.getMessage());
            } else if (ioException instanceof UnknownHostException) {
                System.out.println("Could not load ontology. Unknown host: "
                                   + ioException.getMessage());
            } else {
                System.out.println("Could not load ontology: "
                                   + ioException.getClass().getSimpleName() + " "
                                   + ioException.getMessage());
            }
        } catch (UnparsableOntologyException e) {
            // If there was a problem loading an ontology because there are
            // syntax errors in the document (file) that
            // represents the ontology then an UnparsableOntologyException is
            // thrown
            System.out.println("Could not parse the ontology: "
                               + e.getMessage());
            // A map of errors can be obtained from the exception
            Map<OWLParser, OWLParserException> exceptions = e.getExceptions();
            // The map describes which parsers were tried and what the errors
            // were
            for (OWLParser parser : exceptions.keySet()) {
                System.out.println("Tried to parse the ontology with the "
                                   + parser.getClass().getSimpleName() + " parser");
                System.out.println("Failed because: "
                                   + exceptions.get(parser).getMessage());
            }
        } catch (UnloadableImportException e) {
            // If our ontology contains imports and one or more of the imports
            // could not be loaded then an
            // UnloadableImportException will be thrown (depending on the
            // missing imports handling policy)
            System.out.println("Could not load import: "
                               + e.getImportsDeclaration());
            // The reason for this is specified and an
            // OWLOntologyCreationException
            OWLOntologyCreationException cause = e
                .getOntologyCreationException();
            System.out.println("Reason: " + cause.getMessage());
        } catch (OWLOntologyCreationException e) {
            System.out.println("Could not load ontology: " + e.getMessage());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
