package co.com.music.pruebaconcepto;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import proxymusic.Note;
import proxymusic.ScorePartwise;
import proxymusic.ScorePartwise.Part;
import proxymusic.ScorePartwise.Part.Measure;
import proxymusic.util.Marshalling;

/**
 * Author: Juan Diego Estrada P�rez<br>
 * Universidad EAFIT<br>
 * Ingenier�a de Sistemas<br>
 * Date: 07-Aug-2011 </p> Este programa utiliza la ontolog�a para inferir la
 * tonalidad de una composici�n musical.
 * 
 * 
 */

public class MusicXMLReader {

	public MusicXMLReader() {

	}

	public ArrayList<String> getMusicNotes(String fileName) throws Exception {

		MusicXMLReader mxr = new MusicXMLReader();

		ArrayList<Note> notes = mxr.getMusicXMLNotes(fileName);

		ArrayList<String> notesOWL = mxr.castMusicNotes(notes);

		return notesOWL;
	}

	/**
	 * Retorna una lista con las notas de la partitura musical en XML.
	 * 
	 * @return ArrayList<Note> retorna una lista de objetos tipo nota.
	 * @throws Exception
	 */
	public ArrayList<Note> getMusicXMLNotes(String fileName) throws Exception {

		File xmlFile = new File(fileName);
		InputStream is = new FileInputStream(xmlFile);

		ScorePartwise scorePartwise = Marshalling.unmarshal(is);
		is.close();

		List<Part> parts;
		List<Measure> measures;
		Part part = new Part();
		Measure measure = new Measure();
		Note note = new Note();

		ArrayList<Note> notes = new ArrayList<Note>();

		parts = scorePartwise.getPart();

		for (int i = 0; i < parts.size(); i++) {

			part = parts.get(i);
			measures = part.getMeasure();

			for (int j = 0; j < measures.size(); j++) {

				measure = measures.get(j);

				for (int k = 0; k < measure.getNoteOrBackupOrForward().size(); k++) {

					Object obj = measure.getNoteOrBackupOrForward().get(k);

					if (obj instanceof Note) {

						note = ((Note) obj);
						notes.add(note);
					}
				}
			}
		}
		return notes;
	}

	/**
	 * 
	 * Retorna las notas en formato string, de la composici�n musical en xml.
	 * 
	 * 
	 * @param notes
	 *            , una lista de objetos tipo Nota
	 * @return owlNotes, una lista de tipo String con las notas.
	 */

	private ArrayList<String> castMusicNotes(ArrayList<Note> notes) {

		ArrayList<String> owlNotes = new ArrayList<String>();

		String alter = "";

		for (Note note : notes) {
			if (note.getPitch() != null) {
				alter = note.getPitch().getAlter() == null ? "0" : note
						.getPitch().getAlter().toString();

				if (alter.equals("0")) {
					owlNotes.add(note.getPitch().getStep().toString());
				} else if (alter.equals("1")) {
					owlNotes
							.add(note.getPitch().getStep().toString() + "Sharp");
				} else if (alter.equals("-1")) {
					owlNotes.add(note.getPitch().getStep().toString() + "Flat");
				}
			}
		}
		return owlNotes;
	}
}
